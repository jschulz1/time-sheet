
    //if job is passed in html line, use that job, else use default job (first in database)
    var job = location.search.split('job=')[1]
    if(typeof job === 'undefined'){
       $.ajax({
          type: "GET",
          url: 'php/getDefJob.php',
          async: false,
          success: function (output) {
            job=output;
          },
          error: function(e){
             console.log(e.message);
          }
      });
    };
    $.ajax({
          type: "GET",
          url: 'php/getJobInfo.php?job='+job,
          async: false,
          success: function (output) {
            var obj = jQuery.parseJSON (output);
            hours = obj.hours;
            totalmonth = hours;
            totaljob = obj.totaljob;
            jobstart = new Date(obj.startdate.slice(0,10));
            jobend =  new Date(obj.enddate.slice(0,10));
            faculty = obj.faculty;
            type = obj.type;
          },
          error: function(e){
             console.log(e.message);
          }
    });
