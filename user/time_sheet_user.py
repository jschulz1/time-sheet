#!/usr/bin/python3

from flask import Flask, request, redirect, render_template, jsonify, abort
import sys
sys.path.append("../admin")
from lib_time_sheet import DB
import MySQLdb
import ldap3
import os
import configparser
import json
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
from itertools import zip_longest
from functools import wraps

app = Flask(__name__, static_url_path='')

#read config
c=configparser.ConfigParser()
config = "/etc/time-sheet.conf"
db = DB(config)
c.read(config)
gserv = c.get('GWDGLDAP', 'server')
gsb = c.get('GWDGLDAP', 'sb')
guser = c.get('GWDGLDAP', 'user')
gpasswd = c.get('GWDGLDAP', 'passwd')
sserv = c.get('STUDLDAP', 'server')
ssb = c.get('STUDLDAP', 'sb')

#trying to prevent 404 concerning some static files because of missing "/" at the end of the url
@app.route('/')
def root():
    return redirect("main", code=302)

@app.route('/main')
def main():
    db.ping()
    ldapuser = request.remote_user
    #only use the username itself (foo@TOP.GWDG.DE)
    user = ldapuser.split('@')[0]
    #sort by enddate desc to display the most recent one
    jobs = db.mysqlq('SELECT name, id FROM job WHERE student=%s ORDER BY endDate DESC', [user])
    #check wether there is already a job for this student
    if jobs:
        try:
            current = request.args['job']
            #user can only get his own jobs
            check_auth(current)
        except KeyError:
            current = jobs[0][1]
        #generates exception if current if not a real job
        except IndexError:
            abort (404)
        #get further information
        userdata = db.mysqlq('SELECT firstname, lastname FROM student WHERE username=%s', [user])
        firstname = userdata[0][0]
        lastname = userdata[0][1]
        jobdata = db.mysqlq('SELECT startDate, endDate FROM job WHERE id=%s', [current])
        startdate = jobdata[0][0]
        enddate = jobdata[0][1]
        #get hours per month
        hours_per_month = {}
        for date, hours in db.mysqlq('''SELECT month, hours from working_hours
                                         where job_id = %s''', [current]):
            idx = 12 * (date.year - 1900) + date.month - 1
            hours_per_month[idx] = hours
        #get total time values
        working_hours = db.mysqlq('''SELECT sum(hours) from working_hours where job_id=%s''', [current])[0][0]
        #TODO: Change hours to work when switching to next month within calendar; info: div class "dhx_cal_date" does contain the date dispayed


        return render_template ('scheduler.html', firstname = firstname, lastname = lastname,
                                                  jobs=jobs,
                                                  current=current,
                                                  hours_per_month = json.dumps(hours_per_month),
                                                  working_hours = working_hours,
                                                  startdate = startdate,
                                                  enddate = enddate)
    else:
        #check whether student is in the database, but has no job
        student = db.mysqlq('SELECT email, firstname, lastname from student where username=%s', [user])
        if student:
            return redirect("create_job", code=302) #geht das ohne ganze URL?
        else:
        #check if user is part of gwdg or students ldap and get further information
            if ldapuser.split('@')[1] == 'TOP.GWDG.DE':
                gldap = ldap3.Server(gserv)
                gconn = ldap3.Connection(gldap, user=guser, password=gpasswd)
                gconn.bind()
                gconn.search(gsb, '(uid={})'.format(user), attributes=["mail", "givenname", "sn"])
                mail = gconn.response[0]['attributes']['mail'][0]
                firstname = gconn.response[0]['attributes']['givenname'][0]
                lastname = gconn.response[0]['attributes']['sn'][0]
            else:
                sldap = ldap3.Server(sserv)
                sconn = ldap3.Connection(sldap, user="s.nummath.studit@student.uni-goettingen.de", password="hmPTwbHmd6")
                #sconn = ldap3.Connection(sldap, authentication=ldap3.SASL, sasl_mechanism=ldap3.GSSAPI)
                sconn.bind()
                sconn.start_tls()
                sconn.bind()
                sconn.search(ssb, '(uid={})'.format(user.split('@')[0]), attributes=['mail', 'givenName', 'sn'])
                mail = sconn.response[0]['attributes']['mail'][0]
                firstname = sconn.response[0]['attributes']['givenname'][0]
                lastname = sconn.response[0]['attributes']['sn'][0]
            #create user in database
            db.mysqlq('''INSERT INTO student (username, email, firstname, lastname)
                         VALUES (%s, %s, %s, %s)''', [user, mail, firstname, lastname])
        return redirect("create_job", code=302) #geht das ohne ganze URL?

@app.route('/create_job')
def create_job():
    db.ping()
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    #get student data from database
    userdata = db.mysqlq('''SELECT email, firstname, lastname FROM student
                          WHERE username=%s''', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    #generate next 10 years for jobcreation pulldown menu
    years = []
    sy = datetime.now() - relativedelta(years=1)
    while sy.year <= (datetime.now() + relativedelta(years=10)).year:
        years.append(sy.year)
        sy = sy + relativedelta(years=1)
    return render_template ('create_job.html', user=user, email=email,
                                               firstname=firstname,
                                               lastname=lastname,
                                               years = years)

@app.route('/create_job_hours', methods = ['POST'])
def create_job_hours():
    db.ping()
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    userdata = db.mysqlq('SELECT email, firstname, lastname FROM student WHERE username=%s', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    #get startdate and enddate for job
    try:
        startdate = date(int(request.form['startY']), int(request.form['startM']), int(request.form['startD']))
        enddate = date(int(request.form['endY']), int(request.form['endM']), int(request.form['endD']))
    except ValueError:
        return render_template ('wrong_date.html')
    #get month between startdate and enddate
    months = month_between_startend(startdate, enddate)
    jobname = request.form['jobname']
    faculty = request.form['fac']
    return render_template('create_job_hours.html', user=user, firstname=firstname,
                                                    lastname=lastname, email=email,
                                                    startdate=startdate, enddate=enddate,
                                                    months=months, jobname=jobname,
                                                    faculty=faculty)

@app.route('/create_job_submit', methods = ['POST'])
def create_job_submit():
    db.ping()
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    userdata = db.mysqlq('SELECT email, firstname, lastname FROM student WHERE username=%s', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    #get startdate and enddate as datetime objects
    startdate = datetime.strptime(request.form['startdate'], '%Y-%m-%d')
    enddate = datetime.strptime(request.form['enddate'], '%Y-%m-%d')
    jobname = request.form['jobname']
    faculty = request.form['fac']
    #get month between startdate and enddate
    months = month_between_startend(startdate, enddate)
    #get hours
    hours = []
    for month in months:
        hours.append(request.form['hours-{}'.format(month)])
    try:
        db.mysqlq('START TRANSACTION')
        #insert job
        db.mysqlq('''INSERT INTO job (name, student,faculty, startDate, endDate)
                     VALUES (%s, %s, %s, %s, %s)''', [jobname, user, faculty, startdate, enddate])
        #get unique job_id
        jobid = db.mysqlq('SELECT id FROM job WHERE name=%s and student=%s', [jobname, user])[0][0]
        #insert working hours per month
        for month, hour in zip(months, hours):
            db.mysqlq('''INSERT INTO working_hours (job_id, month, hours)
                       VALUES (%s, %s, %s)''', [jobid,
                                                datetime.strptime(month, '%Y-%m').strftime('%Y-%m-01'),
                                                hour])
        db.mysqlq('COMMIT')
        return render_template('success.html')
    except:
        db.mysqlq('ROLLBACK')
        return render_template ('jobcreation_error.html')


@app.route('/edit_job')
def edit_job():
    db.ping()
    jobid = request.args['job']
    #user can only get his own jobs
    check_auth(jobid)
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    userdata = db.mysqlq('SELECT email, firstname, lastname FROM student WHERE username=%s', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    #get job information from database
    jobdata = db.mysqlq('''SELECT name, startDate, endDate, faculty FROM job
                           WHERE id=%s''', [jobid])
    jobname = jobdata[0][0]
    startdate = jobdata[0][1]
    enddate = jobdata[0][2]
    faculty = jobdata[0][3]
    #generate next 10 years for jobcreation pulldown menu
    years = []
    sy = datetime.now()
    while sy.year <= (datetime.now() + relativedelta(years=10)).year:
        years.append(sy.year)
        sy = sy + relativedelta(years=1)
    return render_template('edit_job.html', email=email, firstname = firstname,
                                            lastname = lastname, jobname = jobname,
                                            startdate = startdate,
                                            enddate = enddate,
                                            years=years, faculty = faculty,
                                            jobid = jobid)

@app.route('/edit_job_hours', methods = ['POST'])
def edit_job_hours():
    db.ping()
    jobid = request.args['job']
    #user can only get his own jobs
    check_auth(jobid)
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    userdata = db.mysqlq('SELECT email, firstname, lastname FROM student WHERE username=%s', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    jobname = request.form['jobname']
    faculty = request.form['fac']
    jobhour = db.mysqlq('SELECT hours FROM working_hours WHERE job_id=%s', [jobid])
    #extract working hours from query
    jobhours = []
    for (hours,) in jobhour:
        jobhours.append(hours)
    startdate = datetime.strptime(request.form['startdate'], '%Y-%m-%d')
    enddate = date(int(request.form['endY']), int(request.form['endM']), int(request.form['endD']))
    months = month_between_startend(startdate, enddate)
    timedata = zip_longest(months, jobhours, fillvalue='')
    return render_template('edit_job_hours.html', email = email, firstname = firstname,
                                                  lastname = lastname, jobname = jobname,
                                                  startdate = startdate.strftime('%Y-%m-%d'),
                                                  enddate = enddate, jobid = jobid,
                                                  timedata = timedata, faculty = faculty)


@app.route('/edit_job_submit', methods = ['POST'])
def edit_job_submit():
    db.ping()
    jobid = request.args['job']
    #user can only get his own jobs
    check_auth(jobid)
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    userdata = db.mysqlq('SELECT email, firstname, lastname FROM student WHERE username=%s', [user])
    email = userdata[0][0]
    firstname = userdata[0][1]
    lastname = userdata[0][2]
    #get startdate and enddate as datetime objects
    startdate = datetime.strptime(request.form['startdate'], '%Y-%m-%d')
    enddate = datetime.strptime(request.form['enddate'], '%Y-%m-%d')
    jobname = request.form['jobname']
    faculty = request.form['fac']
    #get month between startdate and enddate
    months = month_between_startend(startdate, enddate)
    #get working hours for these months
    hours = []
    for month in months:
        hours.append(request.form['hours-{}'.format(month)])
    try:
        #try to prevent inconsistent database state due to connection problems
        db.mysqlq('START TRANSACTION')
        #set new enddate
        db.mysqlq('UPDATE job SET endDate = %s WHERE id = %s', [enddate, jobid])
        #delete old and insert new working hours
        db.mysqlq('DELETE FROM working_hours WHERE job_id=%s', [jobid])
        for month, hour in zip(months, hours):
            db.mysqlq('''INSERT INTO working_hours (job_id, month, hours)
                         VALUES (%s, %s, %s)''', [jobid,
                                                  datetime.strptime(month, '%Y-%m').strftime('%Y-%m-01'),
                                                  hour])
        db.mysqlq('COMMIT')
    except:
        db.mysqlq('ROLLBACK')
        return render_template('editjob_error.html')
    return render_template('success.html')

@app.route('/delete_job')
def delete_job():
    db.ping()
    jobid = request.args['job']
    #user can only get his own jobs
    check_auth(jobid)
    ldapuser = request.remote_user
    user = ldapuser.split('@')[0]
    try:
        db.mysqlq('START TRANSACTION')
        db.mysqlq('DELETE FROM working_hours WHERE job_id=%s', [jobid])
        #TODO: DELETE events where job_id=%s
        db.mysqlq('DELETE FROM events where job_id=%s', [jobid])
        db.mysqlq('DELETE FROM job WHERE id=%s', [jobid])
        db.mysqlq('COMMIT')
    except:
        db.mysqlq('ROLLBACK')
        return render_template('deletejob_error.html')
    return render_template('success.html')


def check_auth(jobid):
    db.ping()
    student = db.mysqlq('select student from job where id = %s', [jobid])[0][0]
    user = request.remote_user.split('@')[0]
    if student != user:
        abort(403)


def verify_user(f):
    @wraps(f)
    def newf(jobid, **kwargs):
        check_auth(jobid)
        return f(jobid, **kwargs)
    return newf


@app.route('/api/dhtmlx/<jobid>')
@verify_user
def api_list_events(jobid):
    eventlist = []
    for eventid, start, end, text in db.mysqlq('select id, start_date, end_date, text from events where job_id = %s', [jobid]):
        eventlist.append({
            'id': eventid,
            'start_date': start.strftime('%Y-%m-%d %H:%M'),
            'end_date': end.strftime('%Y-%m-%d %H:%M'),
            'text': text
        })
    return jsonify(eventlist)


# Scheduler generates id itself, but accept ids returned in response as alternative
@app.route('/api/dhtmlx/<jobid>/<dummy>', methods = ['POST'])
@verify_user
def api_new_event(jobid, dummy):
    data = request.form
    db.mysqlq('insert into events (start_date, end_date, text, job_id) values (%s, %s, %s, %s)',
              [data['start_date'], data['end_date'], data['text'], jobid])
    eventid = db.mysqlq('select last_insert_id()')[0][0]
    return jsonify(action='inserted', tid=eventid)


@app.route('/api/dhtmlx/<jobid>/<eventid>', methods = ['PUT'])
@verify_user
def api_update_event(jobid, eventid):
    data = request.form
    cursor = db.conn.cursor()
    try:
        cursor.execute(
            'update events set start_date=%s, end_date=%s, text=%s where job_id=%s and id=%s',
            [data['start_date'], data['end_date'], data['text'], jobid, eventid])
        if cursor.rowcount == 0:
            abort(404)
    finally:
        cursor.close()
    return jsonify(action='updated')


@app.route('/api/dhtmlx/<jobid>/<eventid>', methods = ['DELETE'])
@verify_user
def api_delete_event(jobid, eventid):
    cursor = db.conn.cursor()
    try:
        cursor.execute('delete from events where job_id=%s and id=%s', [jobid, eventid])
        if cursor.rowcount == 0:
            abort(404)
    finally:
        cursor.close()
    return jsonify(action='deleted')


#create a list with all months between startdate and enddate for unique working times for each month
def month_between_startend(startdate, enddate):
    months = []
    sd = startdate
    while sd.strftime('%Y-%m') <= enddate.strftime('%Y-%m'):
        months.append(sd.strftime('%Y-%m'))
        sd = sd + relativedelta(months=1)
    return months


#app.debug = True
if app.debug:
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)


if __name__ == '__main__':

   app.run()
