<!-- Funktion, die überprüft, ob es den User schon gibt bzw. ob der User bereits einen Job eingetragen hat. Wenn nicht, dann geschieht die Weiterleitung auf create.html-->
<?php
  include_once("php/base.php");
  // flag um zu pruefen, ob es jobs gibt.
  $inside = true;
  try{
    // query whether or not user has been logged in before
    $sql = "select username, firstname, lastname from student where username ='".$student."'";
    $res = $db->query($sql);
    // if not -> insert user into database 
		if($res->num_rows == 0){
      $email = "";
      $firstname = "unknown";
      $lastname = "unknown";
      // set krb5ccname so that connection to student-ldap is possible
      putenv ("KRB5CCNAME=/var/run/ldap_cc");
      // connect to ldap with right parameters
      $ds = ldap_connect("ug-student-vs2.student.uni-goettingen.de");
      ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
      if ($ds) {
        $r=ldap_sasl_bind($ds,NULL,"","GSSAPI");
          if ($r) {
          //search for mail, givenname, lastname
          $sr = ldap_search($ds,"ou=UG-Student Users, dc=student, dc=uni-goettingen, dc=de", "CN=".$student,array("mail","givenname","sn"));
          // entry does not exist -> it is a GWDG-account
          if(ldap_count_entries($ds,$sr)==0){
            $email = $student."@gwdg.de";
          }
          // entry exists -> set mail, givenname, lastname
          else {
            $info = ldap_get_entries($ds, $sr);
            $email = $info[0]["mail"][0];
            $firstname = $info[0]["givenname"][0];
            $lastname = $info[0]["sn"][0];
          }
        }
       ldap_close($ds);
      }
      $sql = "insert into student values ('".$student."','".$email."','".$firstname."','".$lastname."')";
      $db->query($sql);
      // redirect later plz
      $inside = false;
     }
     // user exists
    else if ($res->num_rows == 1){
        $row = $res->fetch_assoc();
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        if ($firstname == "unknown" ) $firstname = $student;
        if ($lastname == "unknown" ) $lastname = "";
      // look if a job exists
      //$sql= "select name from job where student ='".$student."'";
      //$res = $db->query($sql);
      //// if not
      //if ($res->num_rows == 0) {
      //  // set redirect flag
      //  $inside=false;
     /// }
    }
    // redirect to create.php if student or job did not exist
    if ($inside == false) {
      header('Location: https://math.uni-goettingen.de/time-sheet/create.php');
      die();
    }
  	}catch(Exception $e){
     echo "$e->getMessage()";
  }
?>
<!DOCTYPE html> 
<!--PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">

    <title>Enter working hours</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- dhtmlx --> 
    <script src="dhtmlx/codebase/dhtmlxscheduler.js" type="text/javascript"></script>
    <script src="dhtmlx/codebase/ext/dhtmlxscheduler_collision.js"></script>
    <script src="dhtmlx/codebase/ext/dhtmlxscheduler_limit.js"></script>
    <link rel="stylesheet" href="dhtmlx/codebase/dhtmlxscheduler.css" type="text/css"> 
    
</head>
<body>

<!-- navigation bar -->
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- logo , fixed width/margin, class pull-left important -->
          <a class="pull-left" href="index.php"><img style="max-width:35px; margin-top: 5px;" src="svg/clock.svg"></a>
        </div>
        <!-- links to different pages -->
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="create.php">Create New Job</a></li>
            <li><a href="delete.php">Edit/Delete Job</a></li>
            <li><a href="https://gitlab.gwdg.de/jschulz1/time-sheet">Manual</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
</nav>

<div class ="container">
    <div class="row"> 
      <!-- the p class has to exist before, because innerHTML is changed by javascript that is following -->
      <p class="text-left" id="welcome">
      <h3>Welcome <strong>
      <?php echo ($firstname." ".$lastname); ?>
      </strong></h3></p> 


<div class ="container">
    <div class="row">
    <!-- new container listing all available jobs as buttons -->
    <div class="btn-toolbar" data-toggle="buttons">
  <?php
    
    // list all entered jobs for student and get total number of hours 
    $abfrage = "SELECT name FROM job WHERE student = '$student'";
    $ergebnis = $db->query($abfrage);
    $i = 0;
    // for each job, generate a button containing job name with unique id.
    while($row = $ergebnis->fetch_assoc())
    {
      echo "<button type=\"button\" name=\"" . $row['name'] . "\" id =\"select".$row['name']."\" class=\"btn btn-default\">". $row['name'] . "</button>";
      echo "
      <script>
      $('#select".$row['name']."').on('click', function () {
               var uri= encodeURIComponent(this.name);
               $(location).attr('href', \"index.php?job=\"+uri);
               return false;
      });
      </script>";
      $i = $i+1;
    }
  ?>
</div> 
    <script src="js/getjob.js" type="text/javascript"></script>
<p class="text-left" id="jobinfo"></p> 
    <script>
      var echotext = "";
      echotext += "<strong>Start:</strong> "+jobstart.toDateString()+"   <strong>End:</strong> "+jobend.toDateString();
      // set innerhtml of welcome to echotext
      document.getElementById("jobinfo").innerHTML=echotext;
    </script>


<p class="text-left" id="hours"></p> 
<script>
      function getHoursForJob(cdate){
        var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
        var month = monthNames[cdate.getMonth()];
        var evs = scheduler.getEvents();
        var sumjob = 0;
        for (x in evs) {
          sumjob += (evs[x].end_date-evs[x].start_date)/3.6e6;
        }
        // hours that are supposed to be worked in the month of date, and that have already been entered.
        var summonth = 0;
        var evsmonth = scheduler.getEvents(new Date(cdate.getFullYear(),cdate.getMonth(),1),new Date(cdate.getFullYear(),cdate.getMonth()+1,1));
        for (x in evsmonth) {
          summonth += (evsmonth[x].end_date-evsmonth[x].start_date)/3.6e6;
          //console.log((evsmonth[x].end_date-evsmonth[x].start_date)/3.6e6);
        }
        djobend = new Date(jobend);
        djobstart = new Date(jobstart);
        if (djobend.getMonth() == cdate.getMonth()){
          x = new Date(cdate.getFullYear(),cdate.getMonth(),30) ;
          atotal = Math.floor((djobend.getDate()/30)*hours);
        }
        else if (djobstart.getMonth() == cdate.getMonth() &&  djobstart.getFullYear() == cdate.getFullYear() ){
          x = new Date(cdate.getFullYear(),cdate.getMonth(),30) ;
          atotal = Math.floor(( (30-djobstart.getDate())/30 )*hours);
        }
        else {
          atotal = totalmonth;
        }
        // look whether the student entered too few or too many hours for month. If yes -> make hours red.
        var hourtext = "";
        if (sumjob > totaljob || sumjob < totaljob){
          var color = "red"; 
        }
        // else make hours green
        else {
          var color = "green";
        }
        hourtext += "Hours ("+month+"): <strong style=\"color:green\">"+summonth.toFixed(1)+"/"+atotal+"</strong><br/>";
        hourtext += "Hours (total): <strong style=\"color:"+color+"\">"+sumjob.toFixed(1)+"/"+totaljob+"</strong>";
        // set innerhtml of welcome to echotext
        document.getElementById("hours").innerHTML=hourtext
      }
</script>

</div>
</div>


<!-- generate yellow button (btn-warning class) for autogeneration button -->
<!--<button type="button" id="autogen" class="btn btn-warning"> Generate sample timetable
</button>
-->
</div>
</div>

    <!-- script for the click of autogeneration button. -->       
    <script> $('#autogen').on('click', function gen() {
        var datemonth = scheduler.getState().date
        // warn the user that all data for this month is deleted if autogen is selected
        if (confirm ("Warning: By generating a sample timetable for the month " + RegExp.$1 + " all previously entered data will be removed. Proceed?") ){
          // see if jobname is present in html-line or use default job.
          // call generateSampleTimeTable function with correct jobname and spliced date for pseudorandom autogeneration of work dates
                $.ajax({
                    type: "GET",
                    url: 'php/genSampleTimeTable.php?job='+job+"&date="+datemonth.toISOString().slice(0, 10),
                    async: false,
                    success: function (output) {
                       
                    },
                  error: function(e){
                    console.log(e.message);
                  }
                });
                // reload so that changes are depicted correctly
                location.reload(true);
     }
    })
    </script>
    
    <!-- NoScript warning: Functions cannot be properly used and calendar cannot be displayed if any script blocker is on -->
    <noscript> 
    <div class ="container">
    <div class="row">
    <p class="text-left"><h3 style="color:red">You have JavaScript disabled. Please enable JavaScript in order to view the calendar and use the functions.</h3>
    </div>
    </div>
    </noscript>   
    <p>

    <!-- dhtmlx -->
    <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:600px;'>
      <div class="dhx_cal_navline">
          <div class="dhx_cal_prev_button">&nbsp;</div>
          <div class="dhx_cal_next_button">&nbsp;</div>
          <div class="dhx_cal_today_button"></div>
          <div class="dhx_cal_date"></div>
          <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
          <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
          <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
      </div>
      <div class="dhx_cal_header"></div>
      <div class="dhx_cal_data"></div>       
    </div>
    <script type="text/javascript">
    scheduler.config.xml_date="%Y-%m-%d %H:%i"; //correct time format
    scheduler.config.limit_start = jobstart;
    scheduler.config.limit_end = jobend;
    scheduler.config.display_marked_timespans = true;
    scheduler.ignore_week = function(date){
      if (date.getDay() == 0) //hides Sundays
        return true;
    };
    // change hours display when calendar changes
    scheduler.attachEvent("onBeforeViewChange", function (old_mode, old_date, mode, ndate) {
      if (old_date != undefined ) {
        if (ndate.getMonth() != old_date.getMonth()) {
          getHoursForJob(ndate);
        }
      }
      return true;
    }); // reload hours after update TODO: fires too many events!
    scheduler.init('scheduler_here', new Date(),"week"); // week view
    //load events
    scheduler.load("dhtmlx/events.php?job="+job,"xml", getHoursForJob(scheduler.getState().date)); 
    var dp = new dataProcessor("dhtmlx/events.php?job="+job); //two-way connector to php/db
    // reload hours after update
    dp.attachEvent("onAfterUpdateFinish", function () { return getHoursForJob(scheduler.getState().date); });
    dp.init(scheduler);
    </script>
    <script>      
    // initially set hours and welcome string (sadly with polling, because their is no suitable hook)
    sit = window.setInterval(function () {
      st = scheduler.getEvents(); 
      if (st.length > 0 ) {
        getHoursForJob(scheduler.getState().date); 
        window.clearInterval(sit)};
      },1000);
    document.getElementById("select"+job).className = "btn btn-default active";
    </script>


</body>
</html>
