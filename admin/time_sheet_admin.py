#!/usr/bin/python2
# -*- coding: utf-8 -*-

from flask import Flask, render_template, send_from_directory
import MySQLdb
import locale
import ConfigParser
import dateutil.relativedelta
from datetime import datetime
from lib_time_sheet import DB, generate_pdfs_for_job, generate_month_pdfs
from urllib import quote

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

app = Flask(__name__)

config ="/etc/time-sheet.conf"

db = DB(config)

@app.route('/')
def root():
    db.ping()
    faculties = db.mysqlq('SELECT name, id FROM faculty')
    return render_template('index.html', data=faculties)

@app.route('/<fid>')
def active_students(fid=''):
    db.ping()
    #Get every student who has a running contract within a certain faculty; also determine whether the worked minutes in this month are 25% higher or lower than negotiated.
    #TODO: events table is still without certain id. Web-Frontend has to be fixed accordingly
    students = db.mysqlq('''SELECT student.firstname, student.lastname, student.email, student.username
                            FROM student JOIN job ON student.username = job.student
                            WHERE job.faculty = %s
                            AND curdate() between job.startDate and job.endDate
                            ORDER BY student.username''', [fid])

    # students = db.mysqlq('''SELECT student.firstname, student.lastname, student.email, student.username, sum(abs(due - done) > 0.25*due)
    #  from (select job.student, job.name, 60*working_hours.hours as due, sum(timestampdiff(minute, events.start_date, events.end_date)) as done
    #        from (job join events on job.name = events.job and job.student = events.student)
    #        where job.faculty =%s
    #              and date_format(events.start_date, '%%Y-%%m') = date_format(now() - interval 1 month, '%%Y-%%m')
    #              and curdate() between job.startDate and job.endDate
    #        group by job.student, job.name) as s
    #        join student on s.student = student.username
    #  group by student.username
    #  order by student.username''', [fid])
    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])
    if not students:
        return render_template('error.html')
    else:
        return render_template('faculties_active_stud.html', fid=str(fid), students=students,
                                                             facultyname=facultyname[0][0])

@app.route('/<fid>/all-students')
def all_students(fid=''):
    db.ping()
    students = db.mysqlq('''SELECT DISTINCT firstname, lastname, email, username from student
                            JOIN job on job.student = student.username
                            JOIN faculty on faculty.id = job.faculty
                            WHERE faculty.id=%s''', [fid])
    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])
    if not students:
        return render_template('error.html')
    else:
        return render_template('faculties_all_stud.html', fid=str(fid), students=students,
                                                          facultyname=facultyname[0][0])

@app.route('/<fid>/students/<username>')
def student(fid ='', username=''):
    db.ping()
    jobs = db.mysqlq('''SELECT job.name, job.id, DATE(startDate), DATE(endDate), working_hours.hours
                        FROM job JOIN working_hours ON job.id = working_hours.job_id
                        WHERE job.student=%s and job.faculty=%s and working_hours.month=DATE_FORMAT(curdate(), '%%Y-%%m-01')''', [username, fid])
    studname = db.mysqlq('''SELECT firstname, lastname FROM student WHERE username = %s''', [username])
    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])
    return render_template('student.html', username = str(username), fid=str(fid),
                                           jobs = jobs, firstname = studname[0][0],
                                           lastname=studname[0][1], job_id = jobs[0][1],
                                           facultyname = facultyname[0][0])

@app.route('/<fid>/students/<username>/<job_id>')
def job(fid='', username='', job_id=''):
    db.ping()
    email= db.mysqlq('''SELECT email from student join job on job.student = student.username
                      where job.student=%s and job.id=%s''', [username, job_id])
    jobname = db.mysqlq('''SELECT name FROM job WHERE id=%s''', [job_id])

    working_hours_lm = db.mysqlq('''SELECT sum(timestampdiff(minute, start_date, end_date))
                                    FROM events WHERE job_id = %s
                                    AND date_format(start_date, '%%Y-%%m') = date_format(now() - interval
                                    1 month, '%%Y-%%m')''', [job_id])[0][0] or 0

    working_hours_tm = db.mysqlq('''SELECT sum(timestampdiff(minute, start_date, end_date))
                                    FROM events WHERE job_id =%s and
                                    start_date BETWEEN date_format(now(), '%%Y-%%m-01') and
                                    last_day(now()) + INTERVAL 1 day''', [job_id])[0][0] or 0

    working_hours_total= db.mysqlq('''SELECT sum(timestampdiff(minute, start_date,
                                      end_date)) from events where job_id = %s''', [job_id])[0][0] or 0

    due_hours_tm = db.mysqlq('''SELECT working_hours.hours FROM job
                                JOIN working_hours ON job.id = working_hours.job_id
                                WHERE job_id=%s
                                AND working_hours.month=date_format(curdate(), '%%Y-%%m-01')''', [job_id])

    due_hours_lm = db.mysqlq('''SELECT working_hours.hours FROM job
                                JOIN working_hours ON job.id = working_hours.job_id
                                WHERE job_id=%s
                                AND working_hours.month=date_format(curdate() - interval 1 month,
                                                                    '%%Y-%%m-01')''', [job_id])

    due_hours_total = db.mysqlq('''SELECT sum(hours) FROM working_hours
                                    WHERE job_id = %s''', [job_id])

    job_dates= db.mysqlq('''SELECT DATE(startDate), DATE(endDate) FROM job
                            WHERE id=%s''', [job_id])
    d1=job_dates[0][0]
    d2=job_dates[0][1]
    c = dateutil.relativedelta.relativedelta(d2, d1)

    m = round(2*((12*c.years+c.months)+c.days/30.0))/2.0





    studname = db.mysqlq('''SELECT firstname, lastname FROM student WHERE username = %s''', [username])
    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])[0][0]
    mailbody='''Die Uebersicht zu Ihrem Job %s:
Arbeitstunden diesen Monat:	%s / %s
Arbeitsstunden letzten Monat:	%s / %s
Arbeitsstunden insgesamt:	%s / %s''' % (job, round(working_hours_tm/60, 2), due_hours_tm[0][0],
                                          round(working_hours_lm/60, 2), due_hours_lm[0][0],
                                          round(working_hours_total/60, 2), due_hours_total[0][0])
    return render_template('job.html', working_hours_tm = round(working_hours_tm/60, 2),
                                       working_hours_lm = round(working_hours_lm/60, 2),
                                       due_hours_total = due_hours_total[0][0],
                                       due_hours_tm = due_hours_tm[0][0],
                                       due_hours_lm = due_hours_lm[0][0],
                                       job = jobname[0][0],
                                       firstname = studname[0][0], lastname=studname[0][1],
                                       facultyname = facultyname, username = username,
                                       fid = str(fid), job_id = job_id,
                                       working_hours_total = round(working_hours_total/60, 2),
                                       email=email[0][0], mailbody = quote(mailbody))

#WARNING: Keine Ahnung, was ich da machen wollte.
# @app.route("<fid>/students/<username>/<job_id>/hours_edit")
# def job(fid='', username='', job_id=''):
#     db.ping()
#     due_hours = db.mysqlq('''SELECT hours, month from working_hours where job_id=%s''', [job_id])

@app.route("/<fid>/students/<username>/<job_id>/downloads")
def downloads(fid='', username='', job_id=''):
    db.ping()
    dates = set(datetime(d[0].year, d[0].month, 1) for d in db.mysqlq('''SELECT start_date from events where job_id=%s''', [job_id]))
    month = (d.strftime('%Y-%m') for d in sorted(dates))
    lastmonth= db.mysqlq('''SELECT date_format(endDate, '%%Y-%%m') FROM job
                            WHERE id=%s''', [job_id])
    studname = db.mysqlq('''SELECT firstname, lastname FROM student
                            WHERE username = %s''', [username])
    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])[0][0]
    jobname = db.mysqlq('''SELECT name FROM job WHERE id=%s''', [job_id])
    return render_template('downloads.html', month=month, firstname = studname[0][0],
                                             lastname=studname[0][1], job = jobname[0][0],
                                             fid = fid, job_id = job_id,
                                             username = username,
                                             lastmonth=lastmonth[0][0])
#forking that way doesnt work.
#@app.route("/<fid>/students/<username>/<job_id>/downloads/refresh")
#def refresh(fid='', username='', job_id=''):
#    db.fork()
#    db.ping()
#    studname = db.mysqlq('''SELECT firstname, lastname FROM student
#                            WHERE username = %s''', [username])
#    facultyname = db.mysqlq('''SELECT name FROM faculty WHERE id=%s''', [fid])[0][0]
#    jobdata = db.mysqlq('''SELECT name, startDate, endDate FROM job
#                           WHERE id=%s''', [job_id])
#    print('outer ', job_id)
#    generate_pdfs_for_job(db = db, facname = facultyname, firstname = studname[0][0],
#                          lastname = studname[0][1], startdate = jobdata[0][1],
#                          enddate = jobdata[0][2], jobname = jobdata[0][1],
#                          username = username, job_id = job_id)
#    return render_template('refresh.html', job = jobdata[0][0], job_id = job_id,
#                                           firstname = studname[0][0],
#                                           lastname = studname[0][1],
#                                           username = username,
#                                           fid = str(fid))

@app.route("/<fid>/students/<username>/<job_id>/downloads/<ffolder>/<fname>")
def pdf_file(fid='', username='', job_id='', ffolder='', fname=''):
    # path='/var/lib/time-sheet/{}'.format(ffolder)
    return send_from_directory('/var/lib/time-sheet', '{}/{}'.format(ffolder, fname))

@app.route("/images/<fname>")
def image(fname=''):
    return send_from_directory('images', fname)

#app.debug = True
if app.debug:
    from werkzeug.debug import DebuggedApplication
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)


if __name__ == '__main__':
    app.run()
