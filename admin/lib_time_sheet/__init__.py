#!/usr/bin/python2
# -*- coding: utf-8 -*-

import MySQLdb
import configparser
import locale
import subprocess
import datetime
import codecs
from os import mkdir, fork
from jinja2 import FileSystemLoader, Environment
from itertools import count, takewhile
from dateutil.relativedelta import relativedelta
import os.path as path
from os import mkdir, access, R_OK

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

class DB:
    def __init__(self, cfg):
        self.cfg = cfg
        self.connect()

    def connect(self):
        #read config
        config = configparser.ConfigParser()
        config.read(self.cfg)
        sqlhost = config.get('options', 'host')
        sqluser =  config.get('options','user')
        sqlpasswd = config.get('options','passwd')
        sqldb = config.get ('options', 'db')
        self.logfile = config.get('options','logfile')
        self.basepath = config.get('options','basepath')

        # mysql connection
        self.conn = MySQLdb.connect(host = sqlhost,
                                    port = 3307,
                                    user = sqluser,
                                    passwd = sqlpasswd,
                                    use_unicode=True,
                                    charset='utf8',
                                    db = sqldb)
        self.conn.autocommit(True)
        #print('Connection successfully established!')

    def ping(self):
        try:
            self.conn.ping()
        except:
            self.connect()

    def mysqlq(self, *query, **kwargs):
        cursor = self.conn.cursor()
        cursor.execute(*query, **kwargs)
        result = cursor.fetchall()
        cursor.close()
        return result

    def log(self, s):
        with codecs.open(self.logfile, 'a', encoding='utf-8') as f:
            f.write(s)

    def fork(self):
        self.conn.close()
        ret = fork()
        self.connect()
        return ret


def run_pandoc(mdname, template, pdfname):
    jenv = Environment(loader=FileSystemLoader('/var/www/math/time-sheet/templates'))
    with codecs.open(mdname, 'w', encoding='utf-8') as f:
        f.write(jenv.get_template(template).render())
    return subprocess.Popen(["/usr/bin/pandoc", "--tab-stop=8", "--template=/var/www/math/time-sheet/template.latex", mdname, "-o", pdfname])


def fetch_job_month(db, jobname, job_id, username, month):
    """ fetches all events per month and the total number of worked hours"""
    # fetch all events of the given month
    times = db.mysqlq("""SELECT start_date, end_date, text
                         from events where job_id=%s
                         and start_date between DATE(concat(%s, '-01')) and (LAST_DAY(concat(%s, '-01')) + interval 1 day)
                         order by start_date""", [job_id, month, month])
    # get sum of hours for specific of specific student and job
    sumTime = db.mysqlq("""SELECT sum(timestampdiff(minute,start_date,end_date))/60 as summe
                           from events where job_id=%s
                           and start_date between DATE(concat(%s, '-01')) and (LAST_DAY(concat(%s, '-01')) + interval 1 day)""",
                           [job_id, month, month])[0][0]
    hours = db.mysqlq("""SELECT hours from working_hours where job_id=%s
                         AND month=%s""", [job_id, month+"-01"])[0][0]
    if sumTime == None:
        sumTime = 0
    timeslist = []
    for begin, end, desc in times:
        duration = end-begin
        timeslist.append ({'beginday':begin.strftime("%d"), 'begintime':begin.strftime("%H:%M"), 'end':end.strftime("%H:%M"), 'duration':str(duration), 'desc':desc})
    return timeslist, sumTime, hours


def generate_month_pdfs(db, facname, firstname, lastname, startdate, enddate, jobname, username, job_id):
    summary = []

    for cur in takewhile(lambda c: c < enddate,
                         (startdate + relativedelta(months=n) for n in count())):
        month = cur.strftime("%Y-%m")

        times, sumTime, hours = fetch_job_month(db, jobname, job_id, username, month)

        #not needed anymore due to different working_hours per month
        # if (month == startdate.strftime('%Y-%m') and startdate.strftime('%d') > 10 or
        #     month == enddate.strftime('%Y-%m') and enddate.strftime('%d') <= 20):
        #     hours = round(hours / 2, 2)

        summary.append({'month':month,'sumTime':sumTime,'hours':hours})
        if not times:
            continue

        db.log("writing MD-File for month {}\n".format(month))
        monthpath = path.join(db.basepath, month)
        if not access(monthpath, R_OK):
            mkdir(monthpath)
        monthfile = path.join(monthpath, "job_{}_{}".format(jobname, username))

        db.log("converting to PDF\n")
        run_pandoc(monthfile+".md", 'month.md', monthfile+".pdf",
                   jobname=jobname,
                   times=times,
                   firstname=firstname,
                   lastname=lastname,
                   facname=facname,
                   username=username,
                   month=cur.strftime("%B").decode(locale.getlocale()[1]),
                   year=cur.strftime('%Y'),
                   hours=hours,
                   sumTime=round(sumTime, 2)).wait()

    return summary


def generate_pdfs_for_job(db, facname, firstname, lastname, startdate, enddate, jobname, username, job_id):
    print('inner ', job_id)

    summary = generate_month_pdfs(db, facname, firstname, lastname, startdate,
                                  enddate, jobname, username, job_id)

    totalsumTime = sum([int(x['sumTime']) for x in summary])

    totalhours = sum([int(x['hours']) for x in summary])

    # c = relativedelta(enddate, startdate)
    # numbermonths = round(2*((12*c.years+c.months)+c.days/30.0))/2.0
    # totalhours = numbermonths * summary[0]['hours']

    db.log("writing final MD for {}/{}\n".format(username, jobname))
    finaldir = path.join(db.basepath, enddate.strftime("%Y-%m"))
    if not access(finaldir, R_OK):
        mkdir(finaldir)
    finalfile = path.join(finaldir, "job_{}_{}_final".format(jobname, username))

    db.log("converting to PDF\n")
    run_pandoc(finalfile+".md", 'final.md', finalfile+".pdf",
               facname=facname,
               firstname=firstname,
               lastname=lastname,
               username=username,
               jobname=jobname,
               times=summary,
               hours=totalhours,
               sumTime=totalsumTime,
               **template_args).wait()

    return totalsumTime, totalhours
