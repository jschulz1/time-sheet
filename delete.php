<?php

include_once("php/base.php");

?>

<!DOCTYPE html>
<html lang="en"><head>
<!--<html xmlns="http://www.w3.org/1999/xhtml"> -->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Delete Job</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- IE10 vieport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- set styles so that input type text and labels size properly -->
    <style> input[type="text"] { height: 34px } </style>
    <style> label { font-size: 16px} </style>

    </head>
<body>
<!-- navigation bar -->
<nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!--<a class="navbar-brand" href="index.php">Hournator</a>-->
          <a class="pull-left" href="index.php"><img style="max-width:35px; margin-top: 5px;" src="svg/clock.svg"></a>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="create.php">Create New Job</a></li>
            <li><a href="delete.php">Edit/Delete Job</a></li>
            <li><a href="https://gitlab.gwdg.de/jschulz1/time-sheet">Manual</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
</nav>

<!-- form class as std. -->
  <form class="form-horizontal">
   <!-- welcome message -->
   <div class="form-group">
    <label class="col-sm-2 control-label"></label>
<?php
  $ret = getFNLN();
  $firstname =$ret['firstname'];
  $lastname = $ret['lastname'];
      echo "<div class=\"col-sm-10\" id=\"welcome\">
          <h3>Welcome <strong>$firstname $lastname</strong>.</h3>
    </div>";
?>
<!-- blocked javascript warning -> functions will not be available -->
<noscript>
    <label class="col-sm-2 control-label"> </label>
    <div class="col-sm-10">
    <h3 style="color:red">You have JavaScript disabled. Please enable JavaScript in order to view the calendar and use the functions.</h3>
    </div>
</noscript>
  </div>
  <div class="form-group">
    <!-- button toolbar listing all available jobs. -->
    <label for="availJobs" class="col-sm-2 control-label">Jobs:</label>
    <div class="col-sm-10 btn-toolbar" data-toggle="buttons">
       <?php
        // select all jobs. 
        $abfrage = "SELECT name from job where student = '$student'";
        $ergebnis = $db->query($abfrage);
        $i=0;
        // for all jobs : generate job button with unique identifier.
        while($row =$ergebnis->fetch_assoc())
        {
          echo "<button type=\"button\" name=\"". $row['name'] . "\" id =\"select".$row['name']."\" class=\"btn btn-default\">" . $row['name'] ."
          </button>";
      echo "
      <script>
      $('#select".$row['name']."').on('click', function () {
               if ($(\"#success\").is(\":visible\")){
                 $(\"#success\").toggleClass('hidden');
               }
               var uri= encodeURIComponent(this.name);
               var elements = document.querySelectorAll('[id^=select]');
               for (var i = 0, length = elements.length; i < length; i++) {
                 elements[i].className = \"btn btn-default\";
               }
               job=this.name;
               fillJobInfo(this.name);
               return false;
      });
      </script>";
          $i=$i+1;
        }
?>
    <script src="js/getjob.js" type="text/javascript"></script>

    </div>
  </div>
  <!-- show input hours. always shows current jobs hours. is set below via javascript. Changable -->
<div class="form-group">
    <label for="inputHours" class="col-sm-2 control-label">Hours (per month)</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputHours">
    </div>
  </div>
  <!-- change job type. Changable -->
  <div class="form-group">
      <label for="Select" class="col-sm-2 control-label">Job Type</label>
      <div class="col-sm-4">
      <select id="Select" class="form-control">
        <option value="0">Studentische Hilfskraft (SHK)</option>
        <option value="1">Tutor</option>
      </select>
      </div>
</div>
<!-- start date show current one. Is not changeable (disabled) -->
<div class="form-group">
      <label for="start" class="col-sm-2 control-label">Start Date</label>
      <div class="col-sm-1">
      <select id="startD" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
         <option value="16">16</option>
         <option value="17">17</option>
         <option value="18">18</option>
         <option value="19">19</option>
         <option value="20">20</option>
         <option value="21">21</option>
         <option value="22">22</option>
         <option value="23">23</option>
         <option value="24">24</option>
         <option value="25">25</option>
         <option value="26">26</option>
         <option value="27">27</option>
         <option value="28">28</option>
         <option value="29">29</option>
         <option value="30">30</option>
         <option value="31">31</option>
      </select>
      </div>
      <div class="col-sm-2">
      <select id="startM" class="form-control">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      </div>
      <div class="col-sm-1">
      <select id="startY" class="form-control">
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
      </select>
      </div>
</div>
<!-- same for enddate. Changeable -->
<div class="form-group">
      <label for="end" class="col-sm-2 control-label">End Date</label>
      <div class="col-sm-1">
      <select id="endD" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
         <option value="16">16</option>
         <option value="17">17</option>
         <option value="18">18</option>
         <option value="19">19</option>
         <option value="20">20</option>
         <option value="21">21</option>
         <option value="22">22</option>
         <option value="23">23</option>
         <option value="24">24</option>
         <option value="25">25</option>
         <option value="26">26</option>
         <option value="27">27</option>
         <option value="28">28</option>
         <option value="29">29</option>
         <option value="30">30</option>
         <option value="31">31</option>
      </select>
      </div>
      <div class="col-sm-2">
      <select id="endM" class="form-control">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      </div>
      <div class="col-sm-1">
      <select id="endY" class="form-control">
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
      </select>
      </div>
</div>
<!-- faculty. not changeable -->
<div class="form-group">
      <label for="fac" class="col-sm-2 control-label">Faculty</label>
      <div class="col-sm-4">
      <select id="fac" class="form-control" disabled>
        <option value="0">Theologische Fakultät</option>
        <option value="1">Juristische Fakultät</option>
         <option value="2">Medizinische Fakultät</option>
         <option value="3">Philosophische Fakultät</option>
         <option value="4">Fakultät für Mathematik und Informatik</option>
         <option value="5">Fakultät für Physik</option>
         <option value="6">Fakultät für Chemie</option>
         <option value="7">Fakultät für Geowissenschaften und Geographie</option>
         <option value="8">Fakultät für Biologie und Psychologie</option>
         <option value="9">Fakultät für Forstwissenschaften und Waldökologie</option>
         <option value="10">Fakultät für Agrarwissenschaften</option>
         <option value="11">Wirtschaftswissenschaftliche Fakultät</option>
         <option value="12">Sozialwissenschaftliche Fakultät</option>

      </select>
      </div>
</div>
  <!-- update and delete buttons-->
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-3 btn-toolbar" data-toggle="buttons">
    <button type="button" id="updateButton" class="btn btn-warning">Update Job</button>
    <button type="button" id="deleteButton" class="btn btn-danger">Delete Job</button>
  </div>
  </div>
  <!-- normally hidden alert class that shows Success or Fail after changing a job.-->
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-4">
    <div class="alert alert-info hidden" id="success" role="alert">Success!</div> 
  </div>
  </div>

</form>


<script>
function fillJobInfo(job){
    // look if job is passed. if not: search for default job.
      var hours;
      var type;
      var startd;
      var startm;
      var starty;
      var endd;
      var endm;
      var endy;
      var fac;  
      // retrieves job information using current job and student data.
      $.ajax({
        type: "GET",
        url: 'php/getJobInfo.php?job='+job,
        async: false,
        type: "json",
        success: function (output) {
                      var obj= jQuery.parseJSON (output);
                      hours=obj.hours;
                      type=obj.type;
                      startd=obj.startd;
                      startm=obj.startm;
                      starty=obj.starty;
                      endd=obj.endd;
                      endm=obj.endm;
                      endy=obj.endy;
                      fac=obj.fac;
                  },
        error: function(e){
          console.log(e);
        }
      });
      // set the values of form corresponding to the dates there are in the database.
      document.getElementById("startD").value = startd; 
      document.getElementById("startM").value = startm; 
      document.getElementById("startY").value = starty; 
      document.getElementById("endD").value = endd; 
      document.getElementById("endM").value = endm; 
      document.getElementById("endY").value = endy; 
      document.getElementById("fac").value=fac;
      document.getElementById("Select").value=type;
      document.getElementById("inputHours").value=hours;
      document.getElementById("select"+job).className = "btn btn-default active";
}
// do it once when the page is loaded for default job (no job passed to function);
window.onload=fillJobInfo(job);
</script>
    <!-- script for delete button click -->
    <script>
      $('#deleteButton').on('click', function () {
         // hide if alert panel is visible
         if ($("#success").is(":visible")){
                 $("#success").toggleClass('hidden');
         }
         //confirm if job is to be deleted
         if (confirm ("Are you sure you want to delete the job " + job +" ? By deleting it, all dates referenced to it will be removed as well and cannot be reconstructed.")){
        // calling delete job function
          $.ajax({
        type: "POST",
        url: 'php/deletejob.php',
        async: false,
        data: {functionname: 'deleteJob', arguments:[job]},
        success: function (obj, textstatus) {
                  if( !('error' in obj) ) {
                  }
                  else {
                      console.log(obj.error);
                      alert("Deleting from  database failed (unknown reason).");
                      return;
                  }
            }
      });
      // reload, otherwise button row might get holes and stuff.
      $(location).attr('href', 'delete.php');
      return false;
      }
   });
   </script>
    <!-- script for update button click -->
    <script>
      $('#updateButton').on('click', function () {
         // hide if not hidden the alert panel
         if ($("#success").is(":visible")){
                 $("#success").toggleClass('hidden');
         }
         //get all values from the form fields
         var hours= document.getElementById("inputHours").value;
         var type = document.getElementById("Select").value;
         var startd = document.getElementById("startD").value;
         var startm = document.getElementById("startM").value;
         var starty = document.getElementById("startY").value;
         var endd = document.getElementById("endD").value;
         var endm = document.getElementById("endM").value;
         var endy = document.getElementById("endY").value;
         // check_for_correct_hours
         if (hours==""){
           alert('Enter the number of hours you work for this job (per month)')
           return
         }
         //not a number
         if (isNaN(parseInt(hours))){
           alert("Enter an integer number for the hours you work per month.")
           return
         }
         // zero or negative number
         if (parseInt(hours) <=0){
           alert("Job with negative hours or non at all makes no sense.")
           return
         }
         // more than 86 hours (also 86 hours as sum should be forbidden)
         if (parseInt(hours) > 86) {
           alert("You may not work more than 86 hours combined as a student")
           return;
         }
         //construct new startdate,
         var startdate = new Date(startm+"/"+startd+"/"+starty+" 23:59:59")
         //construct new enddate,
         var enddate = new Date(endm+"/"+endd+"/"+endy+" 23:59:59")
         //divide by 1000 to receive php-friendly timestamp.
         startdate = startdate.getTime()/1000;
         enddate = enddate.getTime()/1000;
         // confirm the update
         if (confirm ("Are you sure you want to update the job " + job +" ?")){
            // update the job with new values
            $.ajax({
                type: "GET",
                url: 'php/updateJob.php?job='+job+"&startdate="+startdate+"&enddate="+enddate+"&hours="+hours+"&type="+type,
                async: false,
                type: "json",
                success: function (output) {
                      var obj= jQuery.parseJSON (output);
                      if (obj.error==true){
                        // enddate > old enddate is checked for in php function (otherwise two php calls would be needed)
                        alert ("some error")
                        fillJobInfo(job);
                        return false;
                      }
                      else {
                        // toggle success panel -> so user notices it worked,
                        $("#success").toggleClass('hidden');
                        //alert("Success");
                      }
                      
                },
              error: function(e){
                console.log(e);
              }
            });
            // show new job information after the update
            fillJobInfo(job)
            return false;
          }
   });
   </script>
</body>
</html>
