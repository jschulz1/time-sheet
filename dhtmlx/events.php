<?php
	require_once('codebase/connector/scheduler_connector.php');
  require("codebase/connector/db_mysqli.php");
	include_once ('../php/base.php');

	$scheduler = new schedulerConnector($db,"MySQLi");
  function myInsert($action){
    global $student;
    $action->set_value("job",$_GET['job']);
    $action->set_value("student",$student);
  }
   
  $scheduler->event->attach("beforeInsert","myInsert");
  $scheduler->event->attach("beforeUpdate","myInsert");
  $scheduler->filter("job",$_GET['job']);
  $scheduler->filter("student",$student);
	$scheduler->render_table("events","id","start_date,end_date,text,job,student");
?>
