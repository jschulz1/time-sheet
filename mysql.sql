use hiwis;

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `text` varchar(255) NOT NULL,
  `job` varchar(500) NOT NULL,
  `student` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
  KEY `job` (`job`(255)),
  KEY `student` (`student`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
