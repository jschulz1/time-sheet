<!DOCTYPE html> 
<!-- PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Create new Job </title>
    <!-- Custom styles for this template -->
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <style>.carbonad,
#content > #right > .dose > .dosesingle,
#content > #center > .dose > .dosesingle,
#carbonads-container
{display:none !important;}</style>

<!-- set appropriate text height for inpout type text -->
<style> input[type="text"] { height: 34px } </style>
<!-- set appropriate font-size for labels -->
<style> label { font-size: 16px} </style>
</head>

<body>
    <!-- navigation bar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <!-- <a class="navbar-brand" href="index.php">Hournator</a> -->
         <a class="pull-left" href="index.php"><img style="max-width:35px; margin-top: 5px;" src="svg/clock.svg"></a>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="create.php">Create New Job</a></li>
            <li><a href="delete.php">Edit/Delete Job</a></li>
            <li><a href="https://gitlab.gwdg.de/jschulz1/time-sheet">Manual</a></li>

            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
</nav>
<div class ="container">
</div>
   <!-- use horizontal form as container content -->
   <form class="form-horizontal">
    <!-- first form group -> greet user -->
   <div class="form-group">
    <label class="col-sm-2 control-label"></label>
  <?php
  $student=$_SERVER['PHP_AUTH_USER'];
  $firstname =$student;
  $lastname = "";
  try{
    // connect to student ldap via gssapi
    putenv ("KRB5CCNAME=/var/run/ldap_cc");
    $ds=ldap_connect("ug-student-vs2.student.uni-goettingen.de");
    error_reporting(E_ERROR | E_PARSE);
    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ($ds) {
      $r=ldap_sasl_bind($ds,NULL,"","GSSAPI");
      if ($r){
        // search for student
        $sr=ldap_search($ds,"ou=UG-Student Users, dc=student, dc=uni-goettingen, dc=de", "CN=".$student,array("givenname","sn"));
        #$sr=ldap_search($ds,"ou=UG-Student Users, dc=student, dc=uni-goettingen, dc=de", "CN=kevin.neumann",array("mail"));
        if(ldap_count_entries($ds,$sr)!=0){
          // if found -> set firstname lastname with student info
          $info = ldap_get_entries($ds, $sr);
          $firstname=$info[0]["givenname"][0];
          $lastname=$info[0]["sn"][0];
        }
      }
      ldap_close($ds);
      // if gwdg-account, then just greet with username
      if ($lastname == "") {
        echo "<div class=\"col-sm-10\" id=\"welcome\">
          <h3>Welcome <strong>$firstname</strong>.</h3>
    </div>";
      }
      else {
        //greet with firstname lastname of student
        echo "<div class=\"col-sm-10\" id=\"welcome\">
          <h3>Welcome <strong>$firstname $lastname</strong>.</h3>
    </div>";
      }
    }
  }
  catch(Exception $e){
     echo "$e->getMessage()";
    }

?>
<!-- noscript warning, acknowledging that functions are missing reduced when javascript is blocked -->
<noscript>
    <label class="col-sm-2 control-label"> </label>
    <div class="col-sm-10">
    <h3 style="color:red">You have JavaScript disabled. Please enable JavaScript in order to view the calendar and use the functions.</h3>
    </div>
</noscript>
  </div>
  <div class="form-group">
    <!-- label job name, and input type text for the student to enter his jobname -->
    <label for="inputJob" class="col-sm-2 control-label">Job Name</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputJob" placeholder="Job Name">
    </div>
  </div>
  <!-- label for inputHours, user can input hours per month to be worked -->
  <div class="form-group">
    <label for="inputHours" class="col-sm-2 control-label">Hours (per month)</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="inputHours" placeholder="666">
    </div>
  </div>
  <!-- select job type -->
  <div class="form-group">
      <label for="Select" class="col-sm-2 control-label">Job Type</label>
      <div class="col-sm-4">
      <select id="Select" class="form-control">
        <option value="0">Studentische Hilfskraft (SHK)</option>
        <option value="1">Tutor</option>
      </select>
      </div>
</div>
<!-- select start date of job -->
<div class="form-group">
      <label for="start" class="col-sm-2 control-label">Start Date</label>
      <div class="col-sm-1">
      <select id="startD" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
         <option value="16">16</option>
         <option value="17">17</option>
         <option value="18">18</option>
         <option value="19">19</option>
         <option value="20">20</option>
         <option value="21">21</option>
         <option value="22">22</option>
         <option value="23">23</option>
         <option value="24">24</option>
         <option value="25">25</option>
         <option value="26">26</option>
         <option value="27">27</option>
         <option value="28">28</option>
         <option value="29">29</option>
         <option value="30">30</option>
         <option value="31">31</option>
      </select>
      </div>
      <div class="col-sm-2">
      <select id="startM" class="form-control">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      </div>
      <div class="col-sm-1">
      <input type="text" class="form-control" id="startY"  >
      </select>
      </div>
</div>
<!-- select end date of job -->
<div class="form-group">
      <label for="end" class="col-sm-2 control-label">End Date</label>
      <div class="col-sm-1">
      <select id="endD" class="form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
         <option value="16">16</option>
         <option value="17">17</option>
         <option value="18">18</option>
         <option value="19">19</option>
         <option value="20">20</option>
         <option value="21">21</option>
         <option value="22">22</option>
         <option value="23">23</option>
         <option value="24">24</option>
         <option value="25">25</option>
         <option value="26">26</option>
         <option value="27">27</option>
         <option value="28">28</option>
         <option value="29">29</option>
         <option value="30">30</option>
         <option value="31">31</option>
      </select>
      </div>
      <div class="col-sm-2">
      <select id="endM" class="form-control">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      </div>
      <div class="col-sm-1">
      <input type="text" id="endY" class="form-control">
      </select>
      </div>
</div>
<!-- select faculty you belong to. Math faculty is set per default as wanted -->
<div class="form-group">
      <label for="fac" class="col-sm-2 control-label">Faculty</label>
      <div class="col-sm-4">
      <select id="fac" class="form-control">
        <option value="4">Fakultät für Mathematik und Informatik</option>
        <option value="0">Theologische Fakultät</option>
        <option value="1">Juristische Fakultät</option>
         <option value="2">Medizinische Fakultät</option>
         <option value="3">Philosophische Fakultät</option>
         <option value="5">Fakultät für Physik</option>
         <option value="6">Fakultät für Chemie</option>
         <option value="7">Fakultät für Geowissenschaften und Geographie</option>
         <option value="8">Fakultät für Biologie und Psychologie</option>
         <option value="9">Fakultät für Forstwissenschaften und Waldökologie</option>
         <option value="10">Fakultät für Agrarwissenschaften</option>
         <option value="11">Wirtschaftswissenschaftliche Fakultät</option>
         <option value="12">Sozialwissenschaftliche Fakultät</option>
      </select>
      </div>
</div>


  <!-- submit button -->
  <div class="form-group">
  <label class="col-sm-2 control-label"></label>
  <div class="col-sm-10">
  <button type="submit" id="submitButton" data-loading-text="Loading..."   class="btn btn-primary">Submit</button>
  </div>
  </div>
</form>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- function for click of submit button of data -->
  <script>
    document.getElementById("startY").value =new Date().getFullYear()
    document.getElementById("endY").value =new Date().getFullYear()

    $('#submitButton').on('click', function(){
      var job = $("#inputJob").val()
      var username = ""
      // get username of user
      $.ajax({
        type: "GET",
        url: 'php/getUname.php',
        async: false,
        success: function (output) {
                      username=output;
                  },
        error: function(e){
          console.log(e.message);
        }
      });
      // number of consistency checks, so that no bullshit is entered.
      // kept empty 
      if (job==""){
        alert ('Enter the Job Description')
        return false;
      }
      // job name too long, would not fit in db param
      if (job.length >500) {
        alert('Job Name must not be longer than 500 chars. Who needs that many anyways?')
        return; 
      }
      // check for bad characters in jobname -> disallow as premeasure for injections
      if( /[^a-zA-Z0-9]/.test( job ) ) {
       alert('Job input must be in Alphanumeric form.');
       return;
      }
      // check for hours
      var hours =$("#inputHours").val()
      // hours are empty -> return
      if (hours==""){
        alert('Enter the number of hours you work for this job (per month)')
        return
      }
      // check if entered value is an integer. otherwise -> complain
      // error -> for 22a parses 22. Important? Just recreate if wanted to.
      if (isNaN(parseInt(hours))){
        alert("Enter an integer number for the hours you work per month.")
        return
      }
      // disallow negative or number 0
      if (parseInt(hours) <=0){
        alert("Job with negative hours or non at all makes no sense.")
        return
      }
      // too many hours -> cannot work that much.
      // TODO: check for sum of all jobs available in this month -> sum may not exceed 86 hours.
      if (parseInt(hours) > 86) {
        alert("You may not work more than 86 hours combined as a student")
        return;
      }
      // get values for type, faculty, and selected dates
      var type = $("#Select").val()
      var faculty = $("#fac").val()
      var startday = parseInt($("#startD").val())
      var startmonth = parseInt($("#startM").val())
      var startyear = parseInt($("#startY").val())
      var endday = parseInt($("#endD").val())
      var endmonth = parseInt($("#endM").val())
      var endyear = parseInt($("#endY").val())
      // consistency check for months, that dont have a 31st, and 31st was selected
      if ((startmonth===2 || startmonth===4 || startmonth === 6 || startmonth===9 || startmonth ===11) && startday===31){
        alert("Start month " + startmonth +" has no 31st.");
        return;
      }
      // same for endmonth
      if ((endmonth===2 || endmonth===4 || endmonth === 6 || endmonth===9 || endmonth ===11) && endday===31){
        alert("End month " + endmonth +" has no 31st.");
        return;
      }
      // check specialty of month february
      if (startmonth===2){
        // leap year -> has all days up to 29, but no 30th
        if ((startyear%4===0&&startyear%100!==0) || startyear%400===0){
          if (startday===30){
            alert("Start Month february has no 30th.");
            return;
          }
        }
        // no leap year -> disallow 28th and 29th
        else {
          if (startday===29 || startday===30){
            alert("Start month february has no 29th or 30th if no leap year.");
            return;
          }
        }
      }
      // same stuff for endmonth
      if (endmonth===2){
        if ((endyear%4===0&&endyear%100!==0) || endyear%400===0){
          if (endday===30){
            alert("End Month february has no 30th.");
            return;
          }
        }
        else {
          if (endday===29 || endday===30){
            alert("End month february has no 29th or 30th if no leap year.");
            return;
          }
        }
      }
      // create new dates from entered data
      var startdate = new Date(startmonth+"/"+startday+"/"+startyear)
      // 23:59:59 important, so that dates entered in on last day last month are > 00:00 on the default entry
      var enddate = new Date(endmonth+"/"+endday+"/"+endyear+" 23:59:59")
      // startdate may not be same day or smaller than enddate
      if (startdate >= enddate){
        alert ("Startdate of your job must obviously be before the end of your contract.");
        return;
      }
      // call function to enter job into database. The javascript timestamps have to be divided by 1000 to match php timestamps so that stuff works. 
      $.ajax({
        type: "POST",
        url: 'php/enterjob.php',
        async: false,
        data: {functionname: 'addJob', arguments:[job,username,hours,type,faculty,startdate.getTime()/1000,enddate.getTime()/1000]},
        success: function (obj, textstatus) {
                  if( !('error' in obj) ) {
                  }
                  else {
                      console.log(obj.error);
                      alert("Entering into database failed (unknown reason).");
                      return;
                  }
            }
      });
      // redirect to date entering page if all worked.
      $(location).attr('href', 'index.php');
      return false;
   });
  </script> 

</body></html>
