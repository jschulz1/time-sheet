# Arbeitszeitnachweis: {{jobname}}

**Name, Vorname**: {{lastname}}, {{firstname}}

**Username**: {{username}}

**Fakultät**: {{facname}}

Monat	SollStunden	Stunden
-----	-----------	-------
{% for time in times -%}
{{time.month}}	{{time.hours}}		{{time.sumTime}}
{% endfor %}

**Gesamt**: {{sumTime}}/{{hours}} Stunden

![Max Wardetzky](/var/www/math/time-sheet/signature.png 'Max Wardetzky')
