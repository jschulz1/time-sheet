# Arbeitszeitnachweis: {{jobname}}

**Name, Vorname**: {{lastname}}, {{firstname}}

**Username**: {{username}}

**Fakultät**: {{facname}}

**Monat**: {{month}}   **Jahr**: {{year}}

Datum	Beginn	Ende	Dauer	Bemerkung
-----	------	----	-----	---------
{% for time in times -%}
{{time.beginday}}	{{time.begintime}}	{{time.end}}	{{time.duration}}	{{time.desc}}
{% endfor %}
**Gesamt**: {{sumTime}}/{{hours}} Stunden

![Max Wardetzky](/var/www/math/time-sheet/signature.png 'Max Wardetzky')
