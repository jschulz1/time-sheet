#!/usr/bin/python3

#This is only an one-time script, being pushed to git for having an example for other DB migrations in the future!

import MySQLdb
from datetime import datetime
import sys
sys.path.append("./admin")
import ldap3
import os
import configparser
from lib_time_sheet import DB

c=configparser.ConfigParser()
config="/etc/time-sheet.conf"
db = DB(config)
c.read("/etc/time-sheet.conf")
gserv = c.get('GWDGLDAP', 'server')
gsb = c.get('GWDGLDAP', 'sb')
sserv = c.get('STUDLDAP', 'server')
ssb = c.get('STUDLDAP', 'sb')

#preparing kerberos auth for ldap search
os.environ['KRB5CCNAME'] = "/var/run/ldap_cc"

#delete all accounts and jobs with ’@’ in username;
db.mysqlq('DELETE FROM events where student LIKE "%%@%%"')
db.mysqlq('DELETE FROM job where student LIKE "%%@%%"')
db.mysqlq('DELETE FROM student where username LIKE "%%@%%"')
#delete accounts without username
db.mysqlq('DELETE FROM student where username=""')

#Add id to table "job"
db.mysqlq("ALTER TABLE job ADD id INT AUTO_INCREMENT UNIQUE FIRST")

#Update primary key of table "job"
db.mysqlq("ALTER TABLE job ADD UNIQUE (name, student)")
db.mysqlq("ALTER TABLE job DROP PRIMARY KEY")
db.mysqlq("ALTER TABLE job ADD PRIMARY KEY (id)")

db.mysqlq('ALTER TABLE events ADD job_id INT NOT NULL FIRST')
#Create new table "working_hours"
db.mysqlq("CREATE TABLE working_hours(job_id INT NOT NULL, month DATE NOT NULL, hours INT, FOREIGN KEY (job_id) REFERENCES job (id))")

#Import old jobs to table bla
#get all job ids
jobs = db.mysqlq("SELECT id FROM job")
#iterate over each job
for job_id in jobs:
    #get relevant data via unique job_id
    hours = db.mysqlq("SELECT hours FROM job WHERE id=%s",[job_id])
    start = db.mysqlq("SELECT startDate FROM job WHERE id=%s",[job_id])
    end = db.mysqlq("SELECT endDate FROM job WHERE id=%s",[job_id])
    halfhours = float(hours[0][0])/float(2)
    print(start[0][0])
    print(start[0][0].strftime('%Y-%m-%d'))
    jobname = db.mysqlq('SELECT name FROM job WHERE id=%s', [job_id])
    username = db.mysqlq('SELECT student FROM job WHERE id=%s', [job_id])
    #add job_id to events table
    db.mysqlq('UPDATE events SET job_id=%s WHERE job=%s AND student=%s', [job_id, jobname, username])
    #create a list of month that are part of the contract
    months=db.mysqlq("SELECT month FROM months WHERE month between DATE_FORMAT(%s, '%%Y-%%m-01') AND DATE_FORMAT(%s, '%%Y-%%m-31')", [start[0][0].strftime('%Y-%m-%d'), end[0][0].strftime('%Y-%m-%d')])

    #iterate over every single month
    for month in months:
    #insert only half of the hours worked monthly when the start and/or the end month wasn't completely part of the contract
        if month[0].strftime('%Y-%m')==start[0][0].strftime('%Y-%m') and start[0][0].day > 10:
            db.mysqlq("INSERT INTO working_hours (job_id, month, hours) VALUES (%s, DATE_FORMAT(%s, '%%Y-%%m-01'), %s)", [job_id, start[0][0], halfhours])
        elif (month[0].strftime('%Y-%m') == end[0][0].strftime('%Y-%m')
              and end[0][0].day < 20):
            db.mysqlq("INSERT INTO working_hours (job_id, month, hours) VALUES (%s, DATE_FORMAT(%s, '%%Y-%%m-01'), %s)", [job_id, end[0][0], halfhours])
    #otherwise simply insert standard hours
        else:
            db.mysqlq("INSERT INTO working_hours (job_id, month, hours) VALUES (%s, DATE_FORMAT(%s, '%%Y-%%m-01'), %s)", [job_id, month[0], hours])

#Create job_id column in events table
#db.mysqlq
#Drop "hours" in table "job"
db.mysqlq("ALTER TABLE job DROP COLUMN hours")
    #delete column job and student from events table
db.mysqlq('ALTER TABLE events DROP job, DROP student')

#replace the missing names ("unknown unkown")
for email, username in db.mysqlq('''SELECT email, username FROM student
                                  WHERE firstname="unknown"'''):
  print(username)
  if not username:
      db.mysqlq('DELETE FROM student WHERE username=""')
  emailparts = email.split('@')
  if len(emailparts) > 1 and emailparts[1] == 'gwdg.de':
      gldap = ldap3.Server(gserv)
      gconn = ldap3.Connection(gldap, authentication=ldap3.SASL, sasl_mechanism=ldap3.GSSAPI)
      gconn.bind()
      gconn.search(gsb, '(uid={})'.format(username), attributes=["givenname", "sn"])
      try:
          firstname = gconn.response[0]['attributes']['givenname'][0]
          lastname = gconn.response[0]['attributes']['sn'][0]
      except IndexError:
          firstname = 'deleted'
          lastname = 'user'
      #GWDG-User are only missing first and lastname
      db.mysqlq('''UPDATE student set firstname=%s, lastname=%s
                   WHERE email=%s''', [firstname, lastname, email])
  else:
      #all accounts with missing email are student-accounts
      sldap = ldap3.Server(sserv)
      sconn = ldap3.Connection(sldap, authentication=ldap3.SASL, sasl_mechanism=ldap3.GSSAPI)
      sconn.bind()
      sconn.search(ssb, '(uid={})'.format(username), attributes=['mail', 'givenname', 'sn'])
      mail = sconn.response[0]['attributes']['mail'][0]
      firstname = sconn.response[0]['attributes']['givenname'][0]
      lastname = sconn.response[0]['attributes']['sn'][0]
      db.mysqlq('''UPDATE student SET email=%s, firstname=%s, lastname=%s
                   WHERE username=%s''', [mail, firstname, lastname, username])
