#!/usr/bin/python
# -*- coding: utf-8 -*-

import locale
# import smtplib
# import datetime
import string
import sys
from os import mkdir, access, R_OK
import ConfigParser
import argparse

sys.path.append('./admin')
from lib_time_sheet import DB, generate_pdfs_for_job, generate_month_pdfs

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

parser = argparse.ArgumentParser(description='Creates PDFs of the time-sheets')
parser.add_argument('--config', default="/etc/time-sheet.conf", help='path to configfile')
parser.add_argument('--debug', action="store_true", help='enable debug output and disable sending emails')
args = parser.parse_args()

db = DB(args.config)

#def mailtouser(username,firstname,lastname,email,hours,sumHours,job):
  #send plain text mail as a reminder for the student.
#  if args.debug: email = "schulz@math.uni-goettingen.de"

#  if sumHours == None:
#    sumHours=0
#  von = "time-sheet@math.uni-goettingen.de" #mail addr to be sent from
#  host = "localhost" #mailserver
  # last month
#  month = (datetime.datetime.now()+relativedelta(months=-1)).strftime("%B")
#  subject = ""
#  text = ""
  # Anrede
#  if firstname!="unknown" and firstname != None:
#    text = "Dear " + firstname + " " + lastname
#  else:
#    text = "Dear " + username
  # distinguish if worked too much or less and personalize if student
  # TODO: use jinja templating
#  if hours > sumHours:
#    subject="Too few hours entered for job " + job + " in month " + month
#    text += ".\n\n You only submitted " + str(sumHours) + " of "+ str(hours) + " hours for your job " + job + " in month " + month + ".\n\n Please enter your full amount of hours working so that your time-sheet is finished for the month.\n\n"
#  else:
#    subject="Too many hours entered for job " + job + " in month " + month
#    text += ".\n\n You submitted " + str(sumHours) + " for your job " + job + " in month " + month + ", which exceeds the limit of "+ str(hours) +".\n\n Please delete some of the hours you submitted so that your time-sheet is finished.\n\n"
  #set mail content
#  text += "If you quit this job, please delete it and you won't receive further emails.\n\n    best wishes \n\nNote: This is an automatic generated email"

#  body = string.join((
#    "From: %s" % von,
#    "To: %s" % email,
#    "Subject: %s" % subject,
#    "",
#    text
 # ), "\r\n")
  #connect to server and send mail. if username,password necessary: server.login(username, password)
#  server=smtplib.SMTP(host)
#  server.sendmail(von,[email],body)
#  server.quit()

if not access(db.basepath, R_OK):
  mkdir(db.basepath)

db.log ("*************** \n **** {}\n".format(db.basepath))

for fackey,facname,facmail in db.mysqlq("SELECT id, name, mail FROM faculty"):
  db.log(u"** faculty : {}\n".format(facname))

  jobs = db.mysqlq("""SELECT name, student, firstname, lastname, hours, email, startdate, enddate
                      from job join student on job.student = student.username
                      where startdate < CURRENT_DATE() and faculty=%s""", [fackey])

  for jobname, username, firstname, lastname, hours, email, startdate, enddate in jobs:
    db.log(u"**** user : {}\n".format(username))
    db.log(u"     job : {}\n".format(jobname))
    db.log(u"     enddate : {}\n".format(enddate))
    

    totalsumTime, totalhours = generate_pdfs_for_job(db=db, startdate=startdate, enddate=enddate,
                                                     jobname=jobname, username=username,
                                                     lastname=lastname,
                                                     firstname=firstname,
                                                     facname=facname)

    if not totalsumTime != totalhours:
      db.log("    mail to user {} because {}/{} (TODO)\n".format(username,totalsumTime,totalhours))
      #TODO: mailtouser(username,firstname,lastname,email,totalhours,sumTime,jobname)
