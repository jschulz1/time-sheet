<?php
function getFNLN(){
  $ret = array();
  $student=$_SERVER['PHP_AUTH_USER'];
  $ret["firstname"] = $student;
  $ret["lastname"] = "";
  $ret['error'] = null;
  //TODO: replace with sql-query!
  try{
    ## set krb5ccname for gssapi bind to work
    putenv ("KRB5CCNAME=/var/run/ldap_cc");
    ## connect to ldap
    $ds = ldap_connect("ug-student-vs2.student.uni-goettingen.de");
    error_reporting(E_ERROR | E_PARSE);
    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ($ds) {
      $r = ldap_sasl_bind($ds,NULL,"","GSSAPI");
      if ($r){
        ## search for username in student ldap for firstname lastname
        $sr = ldap_search($ds,"ou=UG-Student Users, dc=student, dc=uni-goettingen, dc=de", "CN=".$student,array("givenname","sn"));
        ## entry found -> student
        if(ldap_count_entries($ds,$sr)!=0){
          $info = ldap_get_entries($ds, $sr);
          $firstname = $info[0]["givenname"][0];
          $lastname = $info[0]["sn"][0];
          ## set firstname lastname
          $ret['firstname'] = $firstname;
          $ret['lastname'] = $lastname;
        }
      }
      ldap_close($ds);
    }
  }
  catch(Exception $e){
     $ret['error'] = $e->getMessage();
    }
  return $ret;
}
## these functions are from wdCalendar. The regex is prone to errors if not used correctly, e.g. you create a date without H:i:d or just Month/Year. Also, slashes have to be included.
function js2PhpTime($jsdate){
  if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
    $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
    //echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  }else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
    $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
    //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  }
  return $ret;
}

function php2JsTime($phpDate){
    //echo $phpDate;
    //return "/Date(" . $phpDate*1000 . ")/";
    return date("m/d/Y H:i", $phpDate);
}

function php2MySqlTime($phpDate){
    return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate){
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);
}

function sqlq($querystring,$db){
  $ret = array();
  try{
    ## simple query based on unique id, just delete
		if($db->query($querystring)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = $db->error . $querystring;
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succesfully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;

}

?>
