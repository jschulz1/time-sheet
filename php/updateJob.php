<?php

## function to update a job (possible to set new enddate, new num. of hours, new type)
function updateJob($job,$dts,$dte,$hours,$type){
# includes dateconversion functions and db connection infos
include_once("base.php");
  $ret = array();
  $ret['error'] = false;
  try{
    # get previous enddate of job
    $sql = "select startdate, enddate from job where student = '".$student."' and name ='".$job."'";
    $handle = $db->query($sql);
    #if(mysql_errno()){
    #$ret['error']= "MySQL error ".mysql_errno().": "
    #     .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
    #}
    while ($row = $handle->fetch_assoc()) {
      $enddate = mySql2PhpTime($row['enddate']);
      if ($enddate>$dte) {
        #delete events which begin after enddate now
        $sql = "delete from events where start_date>'".php2MySqlTime($dte)."' and student ='".$student."' and job ='".$job."'";
        $db->query($sql);
        #  $ret['error']=true;
        #  return $ret;
      }
      $startdate = mySql2PhpTime($row['startdate']);
      if ($startdate<$dts) {
        #delete events which begin before startdate now
        $sql = "delete from events where start_date<'".php2MySqlTime($dts)."' and student ='".$student."' and job ='".$job."'";
        $db->query($sql);
        #delete events which are in between
        #  $ret['error']=true;
        #  return $ret;
      }
    }
    ## update stuff
    $sql = "update job set type=".$type.",hours=".$db->escape_string($hours).",enddate='".php2MySqlTime($dte)."' ,startdate='".php2MySqlTime($dts)."' where student ='".$student."' and name ='".$job."'";
    $handle = $db->query($sql);
    #if(mysql_errno()){
    #$ret['error']= "MySQL error ".mysql_errno().": "
    #     .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
    #}
  }
  catch(Exception $e){
     $ret['error'] = $e->getMessage();
    }
  return $ret;
}
$j=$_GET['job'];
$sd=$_GET['startdate'];
$ed=$_GET['enddate'];
$h=$_GET['hours'];
$t=$_GET['type'];
$ret = updateJob($j,$sd,$ed,$h,$t);
echo json_encode($ret); 

?>
