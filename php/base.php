<?php
  // funktionen und db config laden
  include_once("dbconfig.php");
  include_once("functions.php");
  // get Auth username
  $username = preg_replace('/@.*/', '', $_SERVER['PHP_AUTH_USER']);
  $student = $username;
  // set error reporting
  error_reporting(E_ERROR | E_PARSE);
  try{
    // connect to db
    $db = getConnection();
  }catch(Exception $e){
     echo "$e->getMessage()";
  }
?>
