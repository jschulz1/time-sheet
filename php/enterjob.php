<?php
header('Content-Type: application/json');

## add a new job
function addJob($jobname, $username, $hours, $type,$faculty,$st,$end){
# includes dateconversion functions and db connection infos
include_once("base.php");
  $ret = array();
  try{
    ### username could as well be gotten by php_auth_user...
    ## insert query
    $sql = "insert into `job` (`name`, `student`, `hours`,`type`,`faculty`,`startdate`,`enddate`) values ('"
      .$db->escape_string($jobname)."', '"
      .$db->escape_string($username)."', '"
      .$hours."', '"
      .$type."', '"
      .$faculty."', '"
      .php2MySqlTime($st)."', '"
      .php2MySqlTime($end)."')";
		if($db->query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = $db->error . $sql;
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = $db->insert_id;
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}


## some checks
$aResult = array();

    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

    if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

    if( !isset($aResult['error']) ) {

        switch($_POST['functionname']) {
            case 'addJob':
               if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 5) ) {
                   $aResult['error'] = 'Error in arguments!';
               }
               else {
                   $aResult['result'] = addJob($_POST['arguments'][0],$_POST['arguments'][1],$_POST['arguments'][2],$_POST['arguments'][3],$_POST['arguments'][4],$_POST['arguments'][5],$_POST['arguments'][6]);
               }
               break;
            default:
               $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
               break;
        }

    }

echo json_encode($aResult);
