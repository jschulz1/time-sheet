<?php

# includes dateconversion functions and db connection infos
include_once("base.php");

  $holidays = array();
  # add all holidays of lower saxony in year 2015 and 2016. has to be manually edited.
  ## 2015, obtain dates from feiertage-weltweit.com
  array_push($holidays,DateTime::createFromFormat('m-d-Y','1-1-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','4-3-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','4-6-2015')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-1-2015')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-14-2015')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-25-2015')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','10-3-2015')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-25-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-26-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-27-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-28-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-29-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-30-2015')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-31-2015')->format('Y-m-d'));
  ## 2016
  array_push($holidays,DateTime::createFromFormat('m-d-Y','1-1-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','3-25-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','3-28-2016')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-1-2016')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-5-2016')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','5-16-2016')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','10-3-2016')->format('Y-m-d')); 
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-25-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-26-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-27-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-28-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-29-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-30-2016')->format('Y-m-d'));
  array_push($holidays,DateTime::createFromFormat('m-d-Y','12-31-2016')->format('Y-m-d'));

## function to generate a Sample Time Table
function genSampleTimeTable($db){
$ret = array();
global $holidays;
global $student;
  try{
    ## get job, and date of the month for which autogeneration is wanted.
    $job=$_GET['job'];
    $jsDate=$_GET['date'];
    ## convert to usable format
    $jsDateTS= strtotime($jsDate);
    $dte = date('Y-m-d',$jsDateTS);
    ## gewünschte Studenanzahl für den Job bekommen
    $sql = "select hours, startdate,enddate from job where student ='".$student."' and name = '".$job."'";
    $res = $db->query($sql);
    ## can only be one row, zero impossible bc. of redirect to job generation site
    $row = $res->fetch_assoc();
    ## get infos of job
    $hours = $row['hours'];
    $startdate=mySql2PhpTime($row['startdate']);
    $enddate=mySql2PhpTime($row['enddate']);
    // if not in contract time (between contract start and end) -> do nothing
    if (($dte < date('Y-m-d',$startdate) && date("m",$jsDateTS)!=date("m",$startdate) )|| ($dte > date('Y-m-d',$enddate) && date("m",$jsDateTS)!=date("m",$enddate)  )){
        return $ret;
    }
    ## length of month for which generation is wanted.
    $monthlength=cal_days_in_month(CAL_GREGORIAN,date("m",$jsDateTS),date("Y",$jsDateTS));
    ## copy full hours temporarily
    $roundedhours=$hours;
    ## if autogen wanted for start month or endmonth of contract -> calculate the percantage of hours that have to be worked of the fullhours in case not the whole month is worked.
    if (date("n",$startdate) == date("n",$jsDateTS) && date("Y",$startdate) == date("Y",$jsDateTS)){
        $percentage=(($monthlength-date("j",$startdate))+1)/$monthlength;
        $roundedhours=round($hours*$percentage*2)/2;
      }
      else if (date("n",$enddate) == date("n",$jsDateTS) && date("Y",$enddate) == date("Y",$jsDateTS)){
        $percentage=date("j",$enddate)/$monthlength;
        $roundedhours=round($hours*$percentage*2)/2;
      }
    
    ## per default einfach ganze Stunden nehmen
    if ($hours <=15) {
      $hoursdaily=2;
    }
    else if ($hours <=30) {
      $hoursdaily=3;
    }
    else if ($hours <=45){
      $hoursdaily=4;
    }
    else if ($hours <=60){
      $hoursdaily=5;
    }
    else if ($hours <=75){
      $hoursdaily=6;
    }
    else {
      $hoursdaily=7;
    }
    ## recopy in case roundedhours differs from hours. hours was needed until here to guarantee correct hoursdaily number to disallow 3 days of working possible in a month and 10 hours have to be worked and $hoursdaily=2 f.e. -> endless loop b.c. 6 hours can be worked at max (3 days * 2 hours), but 10 are needed.
    $hours=$roundedhours;
    ## delete all previously entered times of the month
    $sql="delete from events where student = '".$student."' and job = '".$job."' and start_date between date_add(date_add(last_day('".$dte."'),interval 1 DAY),interval -1 month) and date_add(last_day('".$dte."'),interval 1 day)";
    $db->query($sql);
    ## firstday and lastday of the month
    $firstday=date('Y-m-1',strtotime($jsDate));
    $lastday=date('Y-m-t',strtotime($jsDate));
    ## copy startdate, enddate, because variables are reused
    $sd=$startdate;
    $ed=$enddate;
    ## while student has to work
    ### TODO: this function is buggy. it does not seem to enter any dates on the last two days of a month. Why that is so, I don't know. Must be converting/date error or so
    while ($hours>0){
      ## generate randomdate
      $min = strtotime($firstday);
      $max = strtotime($lastday);
      $rnd= mt_rand($min,$max);
      ## 9 o'clock is set as default start time, all 'random' dates start at 9 a.m. to make my life easier.
      $startdate= date('Y-m-d 9:00:00',$rnd);
      ## for half worked months -> check if random dates are in range
      if ($startdate < date('Y-m-d',$sd) || $startdate > date('Y-m-d 23:59:59',$ed)) {
        continue;
      }
      ##autogenerated date is a holiday -> continue
      if (in_array(date('Y-m-d',strtotime($startdate)),$holidays,true)){
        continue;
      }
      ## no work on weekends
      if (date('N', strtotime($startdate)) >= 6){
        continue;
      }
      ## check for double dates
      $sql="select * from events where student ='".$student."' and job ='".$job."' and start_date='".$startdate."'";
      $res=$db->query($sql);
      ## if date already exists -> try again
      if ($res->num_rows!=0){
        continue;
      }
      ### last entry, happens exactly once.
      if ($hours<$hoursdaily){
        ## check if remaining hours is whole number (e.g. 1, 5 or so) -> important for half months or so where part hours that have to be worked can be 15,5 or so.
        if (fmod($hours,1)==0){
          $enddate=date('Y-m-d H:i:s',strtotime($startdate. " +".$hours." hours"));
        }
        ## remaining hours are not in N. 
        else {
          ## get whole hours by flooring, add 30 minutes because only hours possible are 0.5,1,1.5...
          $strippedhours= floor($hours);
          $enddate=date('Y-m-d H:i:s',strtotime($startdate. " +".$strippedhours." hours 30 mins"));
        }
        $sql="insert into events (`text`,`start_date`,`end_date`,`job`,`student`) values ('Work','".$startdate."','".$enddate."','".$job."','".$student."')";
        $db->query($sql);
        $hours=0;
        
      }
      ## hours > hoursdaily, normal case. Just insert.
      else {
        $enddate=date('Y-m-d H:i:s',strtotime($startdate. " +".$hoursdaily." hours"));
        $sql="insert into events (`text`,`start_date`,`end_date`,`job`,`student`) values ('Work','".$startdate."','".$enddate."','".$job."','".$student."')";
        $db->query($sql);
        #subtract hours entered from the total amount of hours
        $hours=$hours-$hoursdaily;
      }
    }
    //echo($sql);
			}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;

}
genSampleTimeTable($db);
?>
