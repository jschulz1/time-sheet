<?php


##for update job -> get all information of job
function getJobInfo($job){
  # includes dateconversion functions and db connection infos
  include_once("base.php");


  $ret = array();
  $ret['hours']=null;
  $ret['type'] = null;
  $ret['startd'] = null;
  $ret['startm'] = null;
  $ret['starty'] = null;
  $ret['endd'] = null;
  $ret['endm'] = null;
  $ret['endy'] = null;
  $ret['fac'] = null;
  $student=$username;
  try{
    ## select all from job. escape is not necessary -> nothing can be entered by user
    $sql = "select hours,startdate,enddate,faculty,type from job where student = '".$student."' and name ='".$job."'";
    $handle = $db->query($sql);
    #if(mysql_errno()){
    #$ret['error']= "MySQL error ".mysql_errno().": "
    #     .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
    #}
    while ($row = $handle->fetch_assoc()) {
      ## set all data
      $ret['hours'] = rtrim($row['hours']);
      $ret['type'] = $row['type'];
      $ret['fac'] = $row['faculty'];
      $ret['startdate'] = $row['startdate'];
      $ret['enddate'] = $row['enddate'];
      $startdate = mySql2PhpTime($row['startdate']);
      $enddate = mySql2PhpTime($row['enddate']);
      $ret['startd'] = date('j',$startdate);
      $ret['startm'] = date('n',$startdate);
      $ret['starty'] = date('Y',$startdate);
      $ret['endd'] = date('j',$enddate);
      $ret['endm'] = date('n',$enddate);
      $ret['endy'] = date('Y',$enddate);
    }
    // TODO: das hier ist falsch
    $numbermonths = floor(($enddate - $startdate)/(60*60*24*30));
    $ret['totaljob'] = $ret['hours']*$numbermonths; 
  }
  catch(Exception $e){
     $ret['error'] = $e->getMessage();
    }
  return $ret;
}
$j = $_GET['job'];
$ret = getJobInfo($j);
echo json_encode($ret); 

?>
