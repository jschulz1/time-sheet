<?php
header('Content-Type: application/json');

## function to delete an entire job (due to database constraints, this automatically leads to the deletion of all dates!)
function deleteJob($jobname){
  # includes dateconversion functions and db connection infos
  include_once("base.php");
  $ret = array();
  # deletes job 
  try{
    ## delete job using primary key
    $sql = "delete from job where name = '"
      .$db->escape_string($jobname)."' and student = '"
      .$db->escape_string($username)."'";
		if($db->query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = $db->error . $sql;
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = $db->insert_id;
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  # deletes events from job
  try{
    ## delete job using primary key
    $sql = "delete from events where job = '"
      .$db->escape_string($jobname)."' and student = '"
      .$db->escape_string($username)."'";
		if($db->query($sql)==false){
      $ret['IsSuccessE'] = false;
      $ret['MsgE'] = $db->error . $sql;
    }else{
      $ret['IsSuccessE'] = true;
      $ret['MsgE'] = 'add success';
      $ret['DataE'] = $db->insert_id;
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}


## caller area with some checks
$aResult = array();

    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

    if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

    if( !isset($aResult['error']) ) {

        switch($_POST['functionname']) {
            case 'deleteJob':
               if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 1) ) {
                   $aResult['error'] = 'Error in arguments!';
               }
               else {
                   $aResult['result'] = deleteJob($_POST['arguments'][0]);
               }
               break;
            default:
               $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
               break;
        }

    }

echo json_encode($aResult);
