<?php

## get the hours needed to work in a month and the hours already entered into the database
function getHours($job,$jsDate){
  # includes dateconversion functions and db connection infos
  include_once("base.php");
  $ret = array();
  ## hours entered
  $ret["done"] =0;
  ## hours needed
  $ret["wanted"] = 0;
  $ret['error'] = null;
  $ret['job'] = $job;
  ## convert date
  $jsDateTS= strtotime($jsDate);
  $dte = date('Y-m-d',$jsDateTS);
  $ret['month']=$dte;
  try{
    ## select hours
    $sql = "select hours,startdate,enddate from job where student = '".$student."' and name ='".$job."'";
    $handle = $db->query($sql);
    #if(mysql_errno()){
    #$ret['error']= "MySQL error ".mysql_errno().": "
    #     .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
    #}
    while ($row = $handle->fetch_assoc()) {
      #get hours and converted start and end dates of contract
      $hours = $row['hours'];
      $startdate = mySql2PhpTime($row['startdate']);
      $enddate = mySql2PhpTime($row['enddate']);
      ## check whether or not month between start and end of contract, else -> set both zero (no need to work)
      if (($dte < date('Y-m-d',$startdate) && date("m",$jsDateTS)!=date("m",$startdate) )|| ($dte > date('Y-m-d',$enddate) && date("m",$jsDateTS)!=date("m",$enddate)  )){
        $ret['wanted']=0;
        $ret['done']=0;
        return $ret;
      }
      #get length of month
      $monthlength=cal_days_in_month(CAL_GREGORIAN,date("m",$jsDateTS),date("Y",$jsDateTS));
      ## if date month/year == first month of contract or last month of contract -> set correct number of wanted hours by using percantages
      if (date("n",$startdate) == date("n",$jsDateTS) && date("Y",$startdate) == date("Y",$jsDateTS)){
        $percentage=(($monthlength-date("j",$startdate))+1)/$monthlength;
        $roundedhours=round($hours*$percentage*2)/2;
        $ret['wanted']=$roundedhours;
      }
      else if (date("n",$enddate) == date("n",$jsDateTS) && date("Y",$enddate) == date("Y",$jsDateTS)){
        $percentage=date("j",$enddate)/$monthlength;
        $roundedhours=round($hours*$percentage*2)/2;
        $ret['wanted']=$roundedhours;
      }
      ## if in between -> wanted hours = full hours
      else {
        $ret['wanted']=$hours;
      }
      #### enddate interval 1 day added so that it includes dates on last day of month (otherwise 31-7 8 a.m > 31-7 0:00 a.m.
      ## get the sum of all dates in the month
      $sql = "select sum(timestampdiff(minute,start_date,end_date))/60 as diff from events where student = '".$student."' and job = '".$job."' and start_date between date_add(date_add(last_day('".$dte."'),interval 1 DAY),interval -1 month) and date_add(last_day('".$dte."'),interval 1 day)";
      $newhndl= $db->query($sql);
      #if(mysql_errno()){
      #   $ret['error']= "MySQL error ".mysql_errno().": "
      #       .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
      #}
      while ($newrow = $newhndl->fetch_assoc()){
        $compare_hours = $newrow['diff'];
        ### if not a single date was entered that month -> break, done remains zero
        if ($compare_hours === NULL) {
          break;
        }
        ## trim entered hours, and set done 
        else {
           $compare_hours= rtrim(rtrim($compare_hours,"0"),".");
           $ret['done'] = $compare_hours;
        }
      }
   	}
  }
  catch(Exception $e){
     $ret['error'] = $e->getMessage();
    }
  return $ret;
}
$j = $_GET['job'];
$m = $_GET['date'];
$ret = getHours($j,$m);
echo json_encode($ret); 

?>
